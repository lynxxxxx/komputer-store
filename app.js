const currentBalanceElement = document.getElementById("balance_amount");
const getLoanElement = document.getElementById("get_loan");
const payAmountElement = document.getElementById("pay_amount");
const sentMonetToBankElement = document.getElementById("sent_bank");
const addWorkMoneyElement = document.getElementById("add_money");
const currentLoanElement = document.getElementById("current_loan");
const repayCurrentLoanElement = document.getElementById("repay_loan");
const laptopsSelectElement = document.getElementById("select_comp");
const laptopsNameElement = document.getElementById("comp_name");
const laptopPriceElement = document.getElementById("comp_price");
const image = document.getElementById("img");
const specsElement = document.getElementById("specs");
const descriptionElement = document.getElementById("description");
const buyLaptopElement = document.getElementById("buy_laptop");

let workAccount = 0;
let bankAccount = 0;
let currentLoan = 0;
let laptops = [];
let deduction = 0;

// here i fetch API converts to json, puts data in laptops array
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((comps) => addLaptopsToMenu(comps));


  //here I go through each computer using the addLoptopToMenu function and set the first elements in the array by default
const addLaptopsToMenu = (laptops) => {
  laptops.forEach((x) => addLaptopToMenu(x));
  laptopsNameElement.innerText = laptops[0].title;
  laptopPriceElement.innerText = laptops[0].price + `Kr`;
  descriptionElement.innerText = laptops[0].description;
  specsElement.innerHTML = laptops[0].specs.join("<br/>");
  image.setAttribute(
    "src",
    `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`
  );
};

//here I get access to single computer
const addLaptopToMenu = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsSelectElement.appendChild(laptopElement);
};

// that function allows you to select another computer from the laptops array
const handleLaptopMenuChange = (e) => {
  const selectLaptop = laptops[e.target.selectedIndex];
  laptopsNameElement.innerText = selectLaptop.title;
  laptopPriceElement.innerText = selectLaptop.price + `Kr`;
  descriptionElement.innerText = selectLaptop.description;
  specsElement.innerHTML = selectLaptop.specs.join("<br/>");

  image.setAttribute(
    "src",
    `https://noroff-komputer-store-api.herokuapp.com/${selectLaptop.image}`
  );
  //imgElement.appendChild(image)
  laptopPrice = selectLaptop.price;
  laptopTitle = selectLaptop.title;
};

//The feature allows you to buy computers if you have enough money
const handleBuyLaptop = () => {
  if (laptopPrice <= bankAccount) {
    alert(`Congratulations you have become the owner of ${laptopTitle}`);
    bankAccount -= laptopPrice;
    currentBalanceElement.innerText = bankAccount;
  } else {
    alert("you do not have enough money in your account");
  }
};

//this feature allows you to get a loan if you do not have a loan and if
// the loans applied for do not exceed your balance * 2
const handleGetLoan = () => {
  if (currentLoan == 0) {
    const getLoan = prompt("enter the amount you want to borrow");
    if (getLoan <= bankAccount * 2 && getLoan > 0) {
      currentLoan += getLoan;
      bankAccount += parseInt(currentLoan);
      currentBalanceElement.innerText = bankAccount;
      currentLoanElement.innerText = `Current loan: ${parseInt(
        currentLoan
      )} Kr`;
      currentLoanElement.style.visibility = "visible";
      repayCurrentLoanElement.style.visibility = "visible";
    } else if (typeof getLoan == "string") {
      alert(
        "The amount exceeds the limit or you have not entered any amount at all"
      );
    } else {
      alert(
        `You already have loan: ${parseInt(
          currentLoan
        )} Kr, Repay the loan first)`
      );
    }
  }
};

// when calling that function, your salary goes up to SEK 100
const handleAddWorkMoney = () => {
  workAccount += 100;
  payAmountElement.innerText = workAccount;
};


//the function allows you to repay the loan if you have a loan,
// the rest of the job money is transferred to your bank balance
const handleRepayLoan = () => {
  if (currentLoan > 0 && workAccount > 0) {
    if (workAccount >= currentLoan) {
      let difference = workAccount - currentLoan;
      bankAccount -= currentLoan;
      bankAccount += difference;
      currentBalanceElement.innerText = parseInt(bankAccount);
      let upDateCurrentLoan = 0;
      currentLoan = upDateCurrentLoan;
      currentLoanElement.innerText = `Current loan: ${parseInt(
        currentLoan
      )} Kr`;
      const workAccountAfterSending = 0;
      workAccount = workAccountAfterSending;
      payAmountElement.innerText = workAccount;
      repayCurrentLoanElement.style.visibility = "hidden";
    } else {
      let difference2 = currentLoan - workAccount;
      currentLoan = difference2;
      bankAccount -= workAccount;
      currentBalanceElement.innerText = parseInt(bankAccount);
      currentLoanElement.innerText = `Current loan: ${parseInt(
        currentLoan
      )} Kr`;
      const workAccountAfterSending = 0;
      workAccount = workAccountAfterSending;
      payAmountElement.innerText = workAccount;
    }
  }
};

//this function allows you to send earned money to your bank acaut and if you have the loan,
// 10% is deducted from sending money and reducing the loans by 10%
const handleSentMoneyToBank = () => {
  deduction = workAccount * 0.1;
  if (currentLoan > 0 && workAccount > 0 && currentLoan > deduction) {
    bankAccount -= currentLoan;
    workAccount -= deduction;
    currentLoan -= deduction;
    bankAccount += workAccount;
    currentBalanceElement.innerText = bankAccount;
    currentLoanElement.innerText = `Current loan: ${parseInt(currentLoan)} Kr`;
    const accountAfterSending = 0;
    workAccount = accountAfterSending;
    payAmountElement.innerText = workAccount;
  } else if (currentLoan > 0 && workAccount > 0 && currentLoan < deduction) {
    workAccount -= deduction;
    currentLoan -= deduction;
    let rest = currentLoan * -1;
    bankAccount += workAccount + rest;
    currentLoan -= currentLoan;
    currentBalanceElement.innerText = bankAccount;
    currentLoanElement.innerText = `Current loan: ${parseInt(currentLoan)} Kr`;
    const accountAfterSending = 0;
    workAccount = accountAfterSending;
    payAmountElement.innerText = workAccount;
  } else if (workAccount > 0 && currentLoan == 0) {
    bankAccount += workAccount;
    currentBalanceElement.innerText = bankAccount;
    const accountAfterSending = 0;
    workAccount = accountAfterSending;
    payAmountElement.innerText = workAccount;
  }
};

addWorkMoneyElement.addEventListener("click", handleAddWorkMoney);
sentMonetToBankElement.addEventListener("click", handleSentMoneyToBank);
getLoanElement.addEventListener("click", handleGetLoan);
repayCurrentLoanElement.addEventListener("click", handleRepayLoan);
laptopsSelectElement.addEventListener("change", handleLaptopMenuChange);
buyLaptopElement.addEventListener("click", handleBuyLaptop);
